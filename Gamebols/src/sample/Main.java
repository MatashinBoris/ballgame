package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {


    @Override
    public void start( Stage primaryStage) throws Exception{
       /*

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();*/

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        Game game = new Game();
        game.start(primaryStage);
    }


    public static void main(String[] args) {
        launch(args);
    }
}

